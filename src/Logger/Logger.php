<?php

namespace Logger;

class Logger
{
    /** @var int  */
    private $index;

    /** @var string  */
    private $log;

    /** @var \DateTime  */
    private $date;

    const error = 'Error';

    const warning = 'Warning';

    const info = 'Info';

    /**
     * Logger constructor.
     * @param string $log
     */
    public function __construct($log = "")
    {
        $this->index = 0;
        $this->log = empty($log) ? "logger.log" : $log;
        $this->date = new \DateTime();
    }

    /**
     * Write in log file
     *
     * @param $message
     * @param $code
     */
    private function writeInLog($message, $code)
    {
        $date = $this->date->format('Y-m-d\TH:i:s.u');
        $logRow = "{$this->index} | $code | $message | $date \n";
        $this->index++;
        try{
            file_put_contents($this->log, $logRow, FILE_APPEND | LOCK_EX);
        } catch (\Exception $exception){
            error_log($exception->getMessage());
        }
    }

    /**
     * Add info row in log file
     *
     * @param $message
     */
    public function addInfo($message)
    {
        $this->writeInLog($message, self::info);
    }

    /**
     * Add warning in log file
     *
     * @param $message
     */
    public function addWarning($message)
    {
        $this->writeInLog($message, self::warning);
    }

    /**
     * Add error in log file
     *
     * @param $message
     */
    public function addError($message)
    {
        $this->writeInLog($message, self::error);
    }
}