<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Logger\Logger;

$logger = new Logger();
$logger->addInfo('This is an info');
$logger->addWarning('This is a warning');
$logger->addError('This is an error');